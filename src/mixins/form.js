import {getById} from "@/api/crud/avueStore";

export default (id,app, option = {}) => {
  let mixins = {
    components:{
      getById,
    },
    data(){
      return{
        value:undefined,
        id:id,
        option:undefined,
      }
    },
    computed:{
      bindVal () {
        return {
          ref: 'form',
          option: this.option,
          data: this.data,
          tableLoading: this.loading
        }
      }
    },
    created() {
      getById(this.id).then(res=>{
        if(res.data.context){
          this.option = JSON.parse(res.data.context)
        }
      })
    }
  }
  app.mixins = app.mixins || [];
  app.mixins.push(mixins)
  return app;
}
