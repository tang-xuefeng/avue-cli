export default (app, option = {}) => {
  let mixins = {
    data () {
      return {
        data: [],
        form: {},
        saveForm:{},
        params: {},
        api: require(`@/api/${option.name}`),
        loading: false,
        page: {},
        args:{},
      }
    },
    computed: {
      option () {
        return require(`@/option/${option.name}`).default(this)
      },
      bindVal () {
        return {
          ref: 'crud',
          option: this.option,
          data: this.data,
          tableLoading: this.loading
        }
      },
      onEvent () {
        return {
          'on-load': this.getList,
          'row-save': this.rowSave,
          'row-update': this.rowUpdate,
          'row-del': this.rowDel,
          'refresh-change': this.refreshChange,
          'search-reset': this.searchChange,
          'search-change': this.searchChange
        }
      },
      rowKey () {
        return option.rowKey || 'id'
      }
    },
    methods: {
      disCol(prop, setting){
        let col =this.findObject(this.option.column,prop)
        if(col){
          let clear = setting['clear']
          if(clear != null){
            this.form[prop] = undefined
          }
          if(setting.clear){
            this.form['column'] = undefined
          }
          let disabled = setting['disabled']
          if(disabled != null){
            col['disabled'] = (disabled === 1)
          }
          let required = setting['required']
          let rules = col['rules']
          if(!rules){
            col['rules'] = []
          }
          if(required != null){
            if(col['rules'].length > 0){
              col['rules'][0] = {required : required === 1,message:'请填写' + col.label}
            }else{
              col['rules'].push({required : required === 1,message:'请填写' + col.label})
            }
          }
        }
      },
      getList () {
        const callback = () => {
          this.loading = true;
          let pageParams = {}
          pageParams[option.pageNumber || 'pageNum'] = this.page.currentPage
          pageParams[option.pageSize || 'pageSize'] = this.page.pageSize
          this.params.args =  Object.assign(this.params.args || {}, this.args)
          const data = Object.assign(pageParams, this.params)
          // this.data = [];
          this.api[option.list || 'page'](data).then(res => {
            this.loading = false;
            let data;
            if (option.res) {
              data = option.res(res.data);
            } else {
              data = res.data
            }
            let total = option.total
            this.page.total = data[ total || 'totalCount'];
            let list = data[option.data] || data.list;
            this.data = list ? list : data;
          }).then(()=>{
            if (this.listAfter) {
              this.listAfter(this.data)
            } else {
              // this.$message.success('数据加载成功')
            }
          })
        }
        if (this.listBefore) {
          this.listBefore()
        }
        callback()
      },
      rowSave (row, done, loading) {
        const callback = () => {
          delete this.form.params;
          let nForm = Object.assign(this.form, this.saveForm);
          this.api[option.add || 'add'](nForm).then((data) => {
            this.getList();
            if (this.addAfter) {
              this.addAfter(data)
            } else {
              this.$message.success('新增成功')
            }
            done();
          }).catch(() => {
            loading()
          })
        }
        if (this.addBefore) {
          this.addBefore()
        }
        callback()
      },
      rowUpdate (row, index, done, loading) {
        const callback = () => {
          if(row.updLock){
            this.$message.warning('数据不允许修改')
            loading()
            return
          }
          delete this.form.params;
          this.api[option.update || 'update'](this.form, index).then((data) => {
            this.getList();
            if (this.updateAfter) {
              this.updateAfter(data)
            } else {
              this.$message.success('更新成功')
            }
            done()
          }).catch(() => {
            loading()
          })
        }
        if (this.updateBefore) {
          this.updateBefore()
        }
        callback()
      },
      rowDel (row, index) {
        const callback = () => {
          this.api[option.del || 'del'](row[this.rowKey], row).then((data) => {
            this.getList();
            if (this.delAfter) {
              this.delAfter(data, row, index)
            } else {
              this.$message.success('删除成功')
            }
          })
        }
        if (this.delBefore) {
          this.delBefore()
          callback()
        } else {
          if(row.delLock){
            this.$message.warning('数据不允许删除')
            return
          }
          this.$confirm(`此操作将永久删除该数据, 是否继续?`, '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            callback()
          })
        }
      },
      searchChange (params, done) {
        if (done) done();
        this.params.args = Object.assign(params,this.args);
        this.page.currentPage = 1;
        this.getList();
      },
      refreshChange () {
        console.log(123)
        this.params.args = this.args
        this.getList();
      }
    }
  }
  app.mixins = app.mixins || [];
  app.mixins.push(mixins)
  return app;
}
