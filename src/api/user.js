import request from '@/router/axios';
import { baseUrl } from '@/config/env';
export const loginByUsername = (acc, password, code, redomStr) => request({
    url: baseUrl + '/user/login',
    method: 'post',
    meta: {
        isToken: false
    },
    data: {
        acc,
        password,
        code,
        redomStr
    }

})

export const getUserInfo = () => request({
    url: baseUrl + '/user/getUserInfo',
    method: 'post'
});

export const refeshToken = () => request({
    url: baseUrl + '/user/refesh',
    method: 'post'
})

export const getMenu = (parentId = 0) => request({
    url: baseUrl + '/menu/getMenuTree',
    method: 'post',
    data: {
      parentId
    }
});

export const getTopMenu = () => request({
    url: baseUrl + '/menu/getTopMenu',
    method: 'post'
});

export const sendLogs = (list) => request({
    url: baseUrl + '/user/sendLogs',
    method: 'post',
    data: list
})

export const logout = () => request({
    url: baseUrl + '/user/logout',
    method: 'post'
})
