import {req} from '@/util/request.js'

export const getlistSimple = (code) => {
  return req("/dicItem/getDicItemsSimple", {code:code})
}

export const getList = (data) => {
  return req("/dicItem/getList", data)
}
