import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/role/getUserPageByRole",data)
}

//保存关系
export const saveRoleUserRel = (roleId,selected,deleted) => req('/role/saveRoleUserRel', {roleId:roleId,selected:selected,deleted:deleted})
