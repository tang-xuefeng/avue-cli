import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/user/getRolePageByUser",data)
}

//保存关系
export const saveUserRoleRel = (userId,selected,deleted) => req('/user/saveUserRoleRel', {userId:userId,selected:selected,deleted:deleted})
