import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/dicItem/getPage",data)
}

export const add = (data) => req('/dicItem/save', data)

export const del = (id) => req('/dicItem/deleteById', {id:id})

export const update = (data) => req('/dicItem/updateById', data)
