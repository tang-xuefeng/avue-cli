import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/menu/getMenuTree",data)
}

export const add = (data) => req('/menu/save', data)

export const del = (id) => req('/menu/deleteById', {id:id})

export const update = (data) => req('/menu/updateById', data)
