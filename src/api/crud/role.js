import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/role/getPage",data)
}

export const getList = (data) => req('/role/getList', data)

export const add = (data) => req('/role/save', data)

export const del = (id) => req('/role/deleteById', {id:id})

export const update = (data) => req('/role/updateById', data)

export const getRoleMenuRel = (roleId) => req('/role/getRoleMenuRel', {roleId:roleId})

export const saveRoleMenuRel = (roleId,selected) => req('/role/saveRoleMenuRel', {roleId:roleId,selected:selected})
