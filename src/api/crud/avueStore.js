import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/avueStore/getPage",data)
}

export const add = (data) => req('/avueStore/save', data)

export const del = (id) => req('/avueStore/deleteById', {id:id})

export const update = (data) => req('/avueStore/updateById', data)

export const getById = (id) => req('/avueStore/getById', {id:id})
