import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/user/getPage",data)
}

export const getList = (data) => req('/user/getList', data)

export const add = (data) => req('/user/save', data)

export const del = (id) => req('/user/deleteById', {id:id})

export const update = (data) => req('/user/updateById', data)
