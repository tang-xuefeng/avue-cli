import {req} from '@/util/request.js'

export const page = (data) => {
  return req("/dic/getPage",data)
}

export const getList = (data) => req('/dic/getList', data)

export const add = (data) => req('/dic/save', data)

export const del = (id) => req('/dic/deleteById', {id:id})

export const update = (data) => req('/dic/updateById', data)
