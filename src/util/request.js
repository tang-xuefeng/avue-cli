import request from '@/router/axios';
import { baseUrl } from '@/config/env';

export const req = (url,data) => request({
  url: baseUrl + url,
  method: 'post',
  data: data

})
