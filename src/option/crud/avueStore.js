export default () => {
  return {
    translate: false,
    selection: false,
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    searchMenuSpan:3,
    maxHeight:590,
    column: [
      {
        label: '分类',
        prop: 'type',
        rules: [
          {
            required: true,
          }
        ],
        type: 'radio',
        border:true,
        search:true,
        dataType: 'number',
        dicUrl: window.$baseUrl+'/dicItem/getDicItemsSimple',
        dicMethod:'post',
        dicQuery:{
          code:'avueType'
        },
      },
      {
        label: '表单ID',
        prop: 'id',
        editDisplay:false,
        addDisplay:false,
        search:true,
      },
      {
        label: '名称',
        prop: 'name',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '备注',
        prop: 'remark',
      },
    ],
  }
}
