export default () => {
  return {
    translate: false,
    searchLabelWidth: 100,
    excelBtn: false,
    labelWidth: 110,
    selection: false,
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    searchMenuSpan:3,
    maxHeight:590,
    column: [
      {
        type: 'radio',
        border:true,
        label: '类别',
        dicUrl: window.$baseUrl+'/dicItem/getDicItemsSimple',
        dicMethod:'post',
        dicQuery:{
          code:'dicType'
        },
        props: {
          label: 'label',
          value: 'value'
        },
        prop: 'type',
        dataType: 'number',
        rules: [
          {
            required: true,
            message: '请填写类型'
          }
        ],
        search:true,
      },
      {
        label: '字典项名称',
        prop: 'name',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '编码代号',
        prop: 'code',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: "备注",
        prop: 'remark',
      },
    ],
  }
}
