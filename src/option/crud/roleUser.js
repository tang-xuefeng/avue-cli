export default () => {
  return {
    translate: false,
    searchLabelWidth: 50,
    searchSpan: 10,
    excelBtn: false,
    selection: true,
    tip: false,
    index: false,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    addBtn: false,
    searchMenuSpan:4,
    menu: false,
    emptyBtn: false,
    maxHeight:590,
    column: [
      {
        type: 'radio',
        label: '勾选',
        prop: 'selected',
        search:true,
        dicUrl: window.$baseUrl+'/dicItem/getDicItemsSimple',
        dicMethod:'post',
        dicQuery:{
          code:'selectType'
        },
        dataType: 'number',
        showColumn: false,
      },
      {
        label: '名称',
        prop: 'name',
        search:true,
      },
      {
        label: '账号',
        prop: 'acc',
        search:true,
      },
    ],
  }
}
