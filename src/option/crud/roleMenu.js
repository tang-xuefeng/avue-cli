import {icons} from "@/store/modules/element-ui-icon";
import { baseUrl } from '@/config/env';

export default () => {
  return {
    translate: false,
    searchLabelWidth: 100,
    excelBtn: false,
    labelWidth: 110,
    selection: false,
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    defaultExpandAll: true,
    searchMenuSpan:3,
    maxHeight:590,
    column: [
      {
        label: '名称',
        prop: 'name',
        align: 'left',
        rules: [
          {
            required: true,
            message: '请填写名称'
          }
        ]
      },
      {
        type: 'radio',
        border:true,
        label: '类型',
        dicData: [
          {
            label: '顶菜单',
            value: 0
          },
          {
            label: '左菜单',
            value: 1
          },
          {
            label: '前端资源',
            value: 2
          }
        ],
        props: {
          label: 'label',
          value: 'value'
        },
        prop: 'type',
        dataType: 'number',
        rules: [
          {
            required: true,
            message: '请填写类型'
          }
        ],
      },
      {
        type: 'tree',
        label: "上级节点",
        prop: 'parentId',
        showColumn:false,
        dicUrl: baseUrl + '/menu/getMenuTree',
        dicQuery:{},
        dicMethod: 'post',
        parent: true,
        props: {
          label: 'label',
          value: 'value'
        },
        rules: [
          {
            required: false,
          }
        ],
      },
      {
        label: '语言标识',
        prop: 'lang',
        align: 'center',
        params: {},
        tipPlacement: 'top',
        labelTip: '用于支持前端国际化功能',
        labelTipPlacement: 'top',
        rules: [
          {
            required: false,
          }
        ],

      },
      {
        type: 'icon',
        label: '图标',
        component: 'avue-input-icon',
        params: {
          iconList: [
            {
              label: '图标',
              list: icons,
            },
          ]
        },
        prop: 'icon',
        rules: [
          {
            required: false,
          }
        ],
      },
      {
        label: '排序',
        prop: 'sort',
        align: 'left',
        type: 'number',
        rules: [
          {
            required: false,
          }
        ],
      },
      {
        label: '路由路径',
        prop: 'path',
        align: 'left',
        rules: [
          {
            required: false,
          }
        ],
      },
      {
        label: '组件路径',
        prop: 'component',
        align: 'left',
        rules: [
          {
            required: false,
          }
        ],
      },
      {
        label: '权限标识',
        prop: 'auth',
        align: 'left',
        rules: [
          {
            required: false,
          }
        ],
      }
    ],
  }
}
