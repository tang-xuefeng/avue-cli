export default () => {
  return {
    translate: false,
    searchLabelWidth: 80,
    excelBtn: false,
    labelWidth: 110,
    selection: false,
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    searchMenuSpan:6,
    maxHeight:590,
    column: [
      {
        label: '名称',
        prop: 'name',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '手机号码',
        prop: 'phone',
        search:true,
      },
      {
        label: '邮箱',
        prop: 'email',
      },
      {
        label: '账号',
        prop: 'acc',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '密码',
        prop: 'pwd',
        rules: [
          {
            required: true,
          }
        ],
      },
      {
        label: '备注',
        prop: 'remark',
      },
    ],
  }
}
