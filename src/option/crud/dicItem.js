export default () => {
  return {
    translate: false,
    searchLabelWidth: 80,
    searchSpan: 6,
    excelBtn: false,
    labelWidth: 110,
    selection: false,
    tip: false,
    index: true,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    searchMenuSpan:6,
    maxHeight:590,
    column: [
      {
        type:'number',
        label: '数字代号',
        prop: 'code',
        editDisabled:true,
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '值',
        prop: 'val',
        rules: [
          {
            required: true,
          }
        ],
        search:true,
      },
      {
        label: '排序',
        prop: 'st',
        type: 'number',
      },
      {
        label: "备注",
        prop: 'remark',
      },
    ],
  }
}
